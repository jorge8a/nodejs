const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
// Iniciamos el servidor aplicacion
const app = express();
// Iniciamos los settings
app.set('port', process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.use(require('./routes/indexController'));
app.use(require('./routes/homeController'));
app.use(require('./routes/logoutController'));
app.use(require('./routes/registrarController'));
app.use(express.static(path.join(__dirname, 'public')));

app.engine('.hbs', exphbs({
    defaultLayout:'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
}));
app.set('view engine', 'hbs');
// Levantamos el server
app.listen( app.get('port'),()=>{
    console.log('El servidor esta corriendo en el puerto ', app.get('port'));
});