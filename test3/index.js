// Incluimos la referencia para trabajar como servidor web
const http = require("http");
const hostname = "127.0.0.1";
const port = 3000;

// Iniciamos el servidor
const server = http.createServer((req, res)=>{

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    var salida='<h1>Mi primer app <br> web con nodejs</h1>';
    res.end(salida);
});

server.listen(port,hostname,()=>{
console.log(`el servidor esta corriendo en http://${hostname}:${port}/`)

});