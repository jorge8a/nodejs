// Incluimos la referencia para trabajar como servidor web
const http = require("http");
const hostname = "127.0.0.1";
const port = 3000;

// Iniciamos el servidor
const server = http.createServer((req, res)=>{

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    var salida='<html><head></head><body><h1>Mi primer app <br> web con nodejs';
    salida+='</h1><img src="https://jorge8a.000webhostapp.com/marvin.png"><h3>Servidor<br><a href="https://github.com/gioback/" target="_blank">Entra a mi github</a><br><a href="https://gitlab.com/jorge8a" target="_blank">Entra a mi gitlab</a></h3></body></html>';
    res.end(salida);
});

server.listen(port,hostname,()=>{
console.log(`el servidor esta corriendo en http://${hostname}:${port}/`)

});